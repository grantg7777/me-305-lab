'''!@file           closedloop.py
    @brief          Implements a closed loop motor controller.
    @details        Allows the user to create a closed loop motor controller
                    object. With only a max speed designation, a closed loop
                    controller can be created. This allows for the motor's duty
                    cycle to be automatically changed until the motor reaches
                    a desired velocity. This is useful for a variety of reasons,
                    the main one being you do not have to do any calculations of
                    what the duty cycle should be to reach a certain speed.
                    
                    The closed loop class has two main methods, one for setting
                    the gains of the controller and one for calculating the
                    required duty cycle to reach the desired speed.
                    
                    To see a class diagram, reference the following:
                        
                    @image html Lab4CLCClass.JPG
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           February 23, 2022
'''

class ClosedLoop:
    '''!@brief               Creates a motor controller object.
        @details             The closed loop class is used for closed loop motor
                             control. Given a max speed designation, a motor
                             controller is created. The controller knows not
                             to try to exceed this max speed. It contains two
                             methods. The first is for running the controller 
                             to calculate the proper duty cycle to reach the 
                             desired speed, and the second is for setting the
                             gains of the controller. There are two gains, Kp
                             and Ki, the former of which is the basic gain that
                             calculates a duty cycle based on the current error,
                             and the latter of which is used for integral control
                             so that the motor will come to the correct steady
                             state value.
        @author              Sean Wahl
        @author              Grant Gabrielson
        @author              Spencer Alonge
        @date                February 23, 2022
    '''
    
    def __init__ (self, maxSpeed):
        '''!@brief          Initializes the motor controller.     
            @details        Given a max speed that the motor cannot exceed, the motor
                            controller is created. It only defines this max speed and
                            some basic variables of the controller that are used in
                            other methods, such as the gains.
            @param maxSpeed The maximum speed that the motor should not go above.
        '''
        
        self.set_gain(0.75, 0.75)
        ## @brief The maximum speed of the motor.
        #
        self.maxSpeed = maxSpeed
        self._errsum = 0
      
        pass
        
    def run (self, wRef, wMeas):
        '''!@brief      Calculates a duty cycle for the motor.    
            @details    Using the desired velocity and the measured velocity,
                        the controller can calculate the required duty cycle
                        necessary to bring the motor up to speed. The reference
                        velocity can not be greater than the max speed or else
                        it will be overrode. This motor controller uses integral
                        control so on top of the basic gain Kp, there is also a
                        Ki used to calculate the duty cycle. This allows for 
                        more accurate steady state control of the motor.
            @param wRef     The desired velocity of the motor.
            @param wMeas    The measured velocity from the encoder.
            @return L       The required duty cycle for the motor.
        '''
        
        # Check if exceeding the max speed.
        if wRef < -self.maxSpeed:
            wRef = -self.maxSpeed
            
        elif wRef > self.maxSpeed:
            wRef = self.maxSpeed
        
        # Basic integral control.
        self._errsum += (wRef-wMeas)
        
        # The temporary duty cycle before adjustment for motor limits.
        self._temp = self.Kp * (wRef-wMeas) + self.Ki * self._errsum / 400
        
        # Final duty cycle after adjusting for motor limits.
        if self._temp > 100:
            ## @brief The duty cycle applied to the motor.
            #
            self.L = 100
        elif self._temp < -100:
            self.L = -100
        else:
            self.L = self._temp
            
        return self.L
    
    def set_gain (self, Kp, Ki):
        '''!@brief      Sets the gain for the controller.   
            @details    Sets the Kp (regular) and Ki (integral) gains for the
                        motor controller. Kp should be in [%s/rad] and Ki should
                        be in [Hz].
            @param Kp   Standard controller gain.
            @param Ki   Integral control gain.
        '''
        # Set gains, set them to 0 if they're set below 0.
        
        ## @brief The Kp gain for the motor controller.
        #
        self.Kp = 0 if Kp < 0 else Kp
        
        ## @brief The Ki gain for the motor controller.
        #
        self.Ki = 0 if Ki < 0 else Ki
        pass
   
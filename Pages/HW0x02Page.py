'''! @page HW0x02       Ball-Balancing Platform Equation Derivation
    @section sec_gen    Overview
                        These are the hand calculations that were completed for homework 0x02 and used for the term project.
                        
                        \htmlonly
                        <center> <embed src="Hw0x02.pdf" width="850px" height="1000px" href="Hw0x02.pdf"></embed> </center>
                        \endhtmlonly
                        
    @author             Grant Gabrielson
    @date               February 11, 2022
'''
'''! @page Lab2Page Lab 2 - Incremental Encoders
    @section sec_gen    Overview
                        In Lab 2, we figured out how to configure encoders 
                        properly to report their position values. We then used
                        this setup to make a general encoder class that can
                        perform several functions. From there, we created two
                        tasks that simulataneously run back and forth to implement
                        a user interface that allowed the user to input specific
                        commands to control the encoder's output. All of this
                        was run from a main file. 
                                        
                        A brief description of all files used are listed below:
                            
                        @ref Lab2Main.py - Called the function files and created 
                                      shared variables used to communicate
                                      between the files. Runs the tasks by
                                      calling for the next input from each.
                                      
                        @ref taskUser.py - Creates a user interface and handles all
                                      user input. Also does all the printing
                                      of data from taskEncoder.
                                      
                        @ref taskEncoder.py - Sets up an Encoder object using the 
                                         encoder class. Transitions between 
                                         states based on what taskUser tells it
                                         to do.
                                         
                        @ref encoder.py - Defines an encoder class with several
                                     functions. Will initialize an encoder if
                                     given proper inputs, and lets the encoder
                                     update, return its position, return its 
                                     last change in position, and zero itself.
                                     See encoder.Encoder for details.
                                     
                        @ref shares.py - Sets up a sharing class in order to create
                                    variables accessible from multiple files.
                                    Also can create queues, however this was not
                                    utilized here. Provided courtesy of Charlie
                                    Refvem at Cal Poly.
                        
    @section sec_STDs   State Transition Diagrams
                        The finite state machines for the generator functions
                        are very important as they represent the logic behind
                        the code we've written.
                        
                        For the taskEncoder.py function, the finite state machine
                        looks like:
                            
                        @image html Lab2EncoderStates.JPG
                        
                        For the taskUser.py function, the finite state machine is:  
                            
                        @image html Lab2UserStates.JPG
                        
    @section sec_ODs   Other Diagrams
                        In addition to the finite state machines we also made a
                        few other diagrams to aid in understanding our code's 
                        structure.
                        
                        The Task Diagram for the main file is as pictured below:
                            
                        @image html Lab2TaskDiagram.JPG
                        
                        The Encoder Class diagram is also featured below:
                            
                        @image html Lab2ClassDiagram.JPG
                        
    @section sec_plot  Data Collection Plot
                        After running our code and collecting data for the
                        full 30 seconds, we were able to create the following
                        plot. Note: the jagged edges of the plot are a product
                        of us manually turning the motor shaft. We cannot
                        move smoothly and require repositioning our hands so
                        there are brief slow periods during these times.
                        
                        @image html Lab2Plot.JPG  
                        
    @author             Sean Wahl
    @author             Grant Gabrielson
    @date               February 2, 2022
'''
'''!@file           Lab5Main.py
    @brief          Calls functions in order to create a working encoder data
                    collector and motor controller.
    @details        Periodically calls taskUser, taskBNO, taskMotor, and taskClosedLoop to 
                    work together using shared variables and generator functions.
                    These shared variables are things like position values or
                    duty cycles that should be transmitted from user input to
                    their designated operating task. The main purpose of 
                    this file is to continuously try to run each task over
                    and over again. The internal task periods will take care
                    of when they should actually run. To stop the program press
                    'Ctrl+C' and to reset the Nucleo press 'Ctrl+D'. For a task
                    diagram, reference the following image:
                        
                    @image html Lab5TD.JPG
                    
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''
import shares, taskUser4, taskBNO, taskMotor3, taskClosedLoop2

## @brief Shared variable to store the angular velocities of the platform.
#
velocity = shares.Share(0)

## @brief Shared variable to store the euler angles of the platform.
#
euler = shares.Share(0)

## @brief Shared variable to set the duty cycle for motor 1 in taskMotor from taskUser.
#
DC1 = shares.Share(0)

## @brief Shared variable to set the duty cycle for motor 2 in taskMotor from taskUser.
#
DC2 = shares.Share(0)

## @brief Shared variable to set the duty cycle of motor 1 in the closed loop control mode.
#
L1 = shares.Share(0)

## @brief Shared variable to set the duty cycle of motor 2 in the closed loop control mode.
#
L2 = shares.Share(0)

## @brief Shared variable to set the Kp gain.
#
Kp = shares.Share(0)

## @brief Shared variable to set the Kd gain.
#
Kd = shares.Share(0)

## @brief Shared variable to set the Ki gain.
#
Ki = shares.Share(0)

## @brief Shared variable to hold the x-axis angular velocity of the platform.
#
wMeas1 = shares.Share(0)

## @brief Shared variable to hold the y-axis angular velocity of the platform.
#
wMeas2 = shares.Share(0)

## @brief Shared variable to hold the x-axis platform angle.
#
thMeas1 = shares.Share(0)

## @brief Shared variable to hold the y-axis platform angle.
#
thMeas2 = shares.Share(0)

## @brief Shared variable to designate when the closed loop control mode should be active.
#
wFlag = shares.Share(False)

## @brief Shared variable to hold the calibration status of the IMU.
#
cal_state = shares.Share([0, 0, 0, 0])

## @brief Shared variable to designate when the IMU has been calibrated.
#
calFlag = shares.Share(False)

## @brief Shared variable to designate when the IMU has calibrated off a file.
#
fileFlag = shares.Share(False)



if __name__ == '__main__':
    ## @brief List containing taskUser, taskBNO, taskMotor, and taskClosedLoop functions.
    #
    tasklist = [taskUser4.taskUserFunction('Task User', 50_000, velocity, DC1, DC2, L1, L2, Kp, Kd, Ki, wMeas1, wMeas2, thMeas1, thMeas2, wFlag, euler, cal_state, calFlag, fileFlag),
                taskBNO.taskBNOFunction('Task BNO', 2_500, cal_state, velocity, euler, calFlag, wMeas1, wMeas2, thMeas1, thMeas2, fileFlag),
                taskMotor3.taskMotorFunction('Task Motor', 2_500, DC1, DC2, wFlag, L1, L2),
                taskClosedLoop2.taskClosedLoopFunction('Task Closed Loop Controller', 2_500, L1, L2, Kp, Kd, Ki, wMeas1, wMeas2, thMeas1, thMeas2)]
    
    while True:
        try:
            for task in tasklist:
                next(task)
        except KeyboardInterrupt:
            break
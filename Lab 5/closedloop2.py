'''!@file           closedloop2.py
    @brief          Implements a closed loop motor controller.
    @details        Allows the user to create a closed loop motor controller
                    object. This allows for the motor's duty cycle to be 
                    automatically changed until the motor reaches a desired 
                    velocity. This is useful for a variety of reasons, the main 
                    one being you do not have to do any calculations of
                    what the duty cycle should be to reach a certain speed.
                    
                    Even though this was created with getting the motor to a
                    certain speed in mind, it is also analogous to applying
                    torques to our balancing platform. The duty cycles now apply
                    necessary torques to counteract platform rotation.
                    
                    The closed loop class has two main methods, one for setting
                    the gains of the controller and one for calculating the
                    required duty cycle to reach the desired speed.
                    
                    For a class diagram, reference the following:
                        
                    @image html Lab5CLCClass.JPG
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 3, 2022
'''

class ClosedLoop:
    '''!@brief               Creates a motor controller object.
        @details             The closed loop class is used for closed loop motor
                             control. It contains two methods. The first is for 
                             running the controller to calculate the proper duty 
                             cycle to reach the desired speed (apply the necessary
                             torque), and the second is for setting the
                             gains of the controller. There are three gains, Kp,
                             Kd, and Ki. These gains allow for proportional, derivative,
                             and integral control.
        @author              Sean Wahl
        @author              Grant Gabrielson
        @author              Spencer Alonge
        @date                March 3, 2022
    '''
    
    def __init__ (self):
        '''!@brief          Initializes the motor controller.     
            @details        Nothing is needed to be inputted to create the motor
                            controller, however other methods need to be called
                            in order to actually use it.
        '''
        
        self._Kilast = 0
        ## @brief The Ki gain for the motor controller.
        #
        self.Ki = 0
        self._errsum = 0
        
        self.set_gain(0,0,0)
        pass
        
    def run (self, thMeas, wMeas):
        '''!@brief      Calculates a duty cycle for the motor.    
            @details    Using the platform's angle and the measured velocity,
                        the controller can calculate the required duty cycle
                        necessary to bring the platform to level. The reference
                        velocity and angle are both zero in this case, as this
                        indicates a level platform. This motor controller uses integral
                        control so on top of the basic gains Kp and Kd. The 
                        Ki used to calculate the duty cycle allows for the
                        platform to reach complete levelness eventually.
            @param thMeas     The measured angle from the IMU.
            @param wMeas      The measured velocity from the IMU.
            @return L         The required duty cycle (torque) for the motor.
        '''
        
        # Basic integral control.
        self._errsum += -thMeas
        self._errsum = 0 if thMeas == 0 else self._errsum
        
        # The temporary duty cycle before adjustment for motor limits.
        self._temp = -self.Kp * (thMeas) - self.Kd * wMeas + self.Ki * self._errsum/400
        
        self._errsum
        
        # Final duty cycle after adjusting for motor limits.
        if self._temp > 50:
            ## @brief The duty cycle applied to the motor.
            #
            self.L = 50
        elif self._temp < -50:
            self.L = -50
        else:
            self.L = self._temp
            
        return self.L
    
    def set_gain (self, Kp, Kd, Ki):
        '''!@brief      Sets the gain for the controller.   
            @details    Sets the Kp (proportional), Kd (derivative), and Ki 
                        (integral) gains for the motor controller. Kp should 
                        be in [%/deg], Kd should be in [%*s/deg], and Ki should
                        be in [Hz].
            @param Kp   Proportional gain.
            @param Kd   Derivative gain.
            @param Ki   Integral gain.
        '''

        #Set past Ki gain
        self._Kilast = self.Ki
        
        # Set gains, set them to 0 if they're set below 0.
        ## @brief The Kp gain for the motor controller.
        #
        self.Kp = 0 if Kp < 0 else Kp
        
        ## @brief The Kd gain for the motor controller.
        #
        self.Kd = 0 if Kd < 0 else Kd
        
        self.Ki = 0 if Ki < 0 else Ki
        
        self._errsum = 0 if self.Ki != self._Kilast else self._errsum
        pass
   
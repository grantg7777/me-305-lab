"""!@file       BNO055.py
    @brief      Implements an inertial measurement unit.
    @details    The IMU object created is able to measure several kinematic
                values. It's methods allow for it to be calibrated (either
                manually or from a file), retrieve euler angles, or retrieve
                angular velocities. These values are helpful if you are trying
                to do something like balance a platform.     
                
                For a class diagram, reference the following:
                    
                @image html Lab5BNOClass.JPG
    @author     Sean Wahl
    @author     Grant Gabrielson
    @author     Spencer Alonge
    @date       March 3, 2022
"""

from pyb import I2C
from time import sleep_us
import micropython, struct

class BNO055:
    '''!@brief      Creates an IMU object.         
        @details    Nothing is needed to set up the IMU object. It automatically
                    hooks up to the Nucleo's I2C channel and is able to retrieve
                    data from an already connected IMU. After being instantiated,
                    the IMU object can output it's calibration status, read
                    calibration coefficients from a file, write working calibration
                    coefficients to a file, and retrieve euler angles and angular
                    velocities from the IMU. The object also has a method to change
                    it's operating mode, but this should be done according
                    to the data sheet for the IMU. Most of the time NDOF mode is
                    all we will need.
        @author     Sean Wahl
        @author     Grant Gabrielson
        @author     Spencer Alonge
        @date       March 3, 2022
    '''
    
    def __init__ (self):
        '''!@brief      Initializes the IMU object.     
            @details    Sets up the IMU object with I2C communication. It also
                        sets up the address to communicate to it with, and also
                        several IMU specific addresses, like where to retrieve
                        euler angles and angular velocities from and also where
                        to change the operating mode or check the calibration
                        status.
        '''
        ## I2C channel for communication with the IMU.
        #
        self.i2c = I2C(1, I2C.CONTROLLER)
        
        ## I2C address to designate communication with the IMU.
        #
        self.addr = self.i2c.scan()[0]
        
        # Useful IMU internal addresses.
        self._OPR_MODE         = micropython.const(0x3D)
        self._CALIB_STAT       = micropython.const(0x35)
        self._ACC_OFFSET_X_LSB = micropython.const(0x55)
        self._EUL_Heading_LSB  = micropython.const(0x1A)
        self._GYR_DATA_X_LSB   = micropython.const(0x14)
        
        self.op('CONFIGMODE')
        pass
        
    def op (self, mode):
        '''!@brief          Changes the operating mode of the IMU. 
            @details        By specifying which mode is desired to be transitioned to,
                            the op method is able to change the IMU's state.
                            Delays are built in as prescribed by the data sheet,
                            and a dictionary has been added within the code so
                            the user just has to input the operating mode's name
                            in order for the data to be interpreted correctly.
            @param mode     The desired operating mode (must be in apostrophes).
                            Please check with the data sheet for acceptable
                            operating modes.
        '''
        modes = {'CONFIGMODE'   : 0b0000,
                 'ACC_ONLY'     : 0b0001,
                 'MAG_ONLY'     : 0b0010,
                 'GYRO_ONLY'    : 0b0011,
                 'ACC_MAG'      : 0b0100,
                 'ACC_GYRO'     : 0b0101,
                 'MAG_GYRO'     : 0b0110,
                 'AMG'          : 0b0111,
                 'IMU'          : 0b1000,
                 'COMPASS'      : 0b1001,
                 'M4G'          : 0b1010,
                 'NDOF_FMC_OFF' : 0b1011,
                 'NDOF'         : 0b1100}
        
        self.i2c.mem_write(modes['CONFIGMODE'], self.addr, self._OPR_MODE)
        sleep_us(8_000)
        if mode != 'CONFIGMODE':
            self.i2c.mem_write(modes[mode], self.addr, self._OPR_MODE)
            sleep_us(20_000)
        pass
    
    def cal_status (self):
        '''!@brief      Retrieves the calibration statuses of the IMU components.
            @details    The IMU contains a magnetometer, gyroscope, and accelerometer.
                        Before these can be used, they must be properly calibrated.
                        Please reference the calibration video for the BNO055
                        online for how to do this. The IMU must be in NDOF mode
                        to get all components to return calibration statuses.
                        The system calibration status is reflective of all components
                        being calibrated but is sometimes defective.
            @return     The calibration statuses of the IMU components in the form
                        [sys, gyr, acc, mag,] where 0 indicates not calibrated
                        and 3 indicates comletely calibrated.
        '''
        cal_byte = self.i2c.mem_read(1, self.addr, self._CALIB_STAT)[0]
        mag_stat = cal_byte & 0b00000011
        acc_stat = (cal_byte & 0b00001100)>>2
        gyr_stat = (cal_byte & 0b00110000)>>4
        sys_stat = (cal_byte & 0b11000000)>>6
        
        ## Calibration state of the IMU components.
        #
        self.CAL_STATE = [sys_stat, gyr_stat, acc_stat, mag_stat]
        return self.CAL_STATE
    
    #Read from board, write to file
    def cal_read (self, filename):
        '''!@brief             Writes calibration coefficients to a file. 
            @details           Once the IMU is successfully calibrated, this
                               method can be called to write the internal calibration
                               coefficients to a text file for future use.
            @param filename    Desired filename for calibration coefficient text file.
        '''
        self.op('CONFIGMODE')
        
        cal_coef = []
        
        buf = self.i2c.mem_read(22, self.addr, self._ACC_OFFSET_X_LSB)
        
        for byte in buf:
            cal_coef.append(hex(byte))
            
        with open(filename,'w') as f:
            f.write(','.join(cal_coef))
            
        self.op('NDOF')
        
        pass
    
    #Write to board, read from file
    def cal_write (self, filename):
        '''!@brief          Writes calibration coefficients to the IMU.
            @details        If a calibration coefficient text file exists, they can
                            be read from the file and written to the board so the
                            calibration process doesn't have to occur many times.
            @param filename Name of the file with the calibration coefficients.
        '''
        self.op('CONFIGMODE')
        
        cal_coef = []
        
        with open(filename, 'r') as f:
            buf = f.read()
            
        buf2 = buf.split(",")

        for byte in buf2:
            cal_coef.append(int(byte, 16))
            
        byte_array = bytearray(cal_coef)
            
        self.i2c.mem_write(byte_array, self.addr, self._ACC_OFFSET_X_LSB)
        
        self.op('NDOF')
        pass
    
    def euler (self):
        '''!@brief      Retrieves euler angles from the IMU.
            @details    Outputs the euler angles representing the current
                        orientation of the IMU. Be careful about axes, as the
                        board's axes are specifically defined but in this method
                        we correct them to be in line with our platform's axes
                        so that they can be more easily interpreted. 
            @return     IMU's euler angles in degrees, [thx,thy,thz]
        '''
        buf = self.i2c.mem_read(6, self.addr, self._EUL_Heading_LSB)
        
        eul_1, eul_2, eul_3 = struct.unpack('<hhh', buf)
        #From BNO to board axes
        
        ## Euler angles from the IMU.
        #
        self.eul = [-eul_2/16+2, -eul_3/16-1, -eul_1/16+360]
        return self.eul
    
    def ang_vel (self):
        '''!@brief      Retrieves angular velocities from the IMU.
            @details    Outputs the angular velocities corresponding to the rate
                        of rotation of the IMU chip. Again, these values have
                        been manipulated to correspond to our platform's axes.
            @return     IMU's angular velocities in degrees per second [wx, wy, wz]
        '''
        buf = self.i2c.mem_read(6, self.addr, self._GYR_DATA_X_LSB)
    
        angv_1, angv_2, angv_3 = struct.unpack('<hhh', buf)
        
        ## Angular velocities from the IMU.
        #
        self.vel = [angv_2/16, -angv_1/16, angv_3/16]
        return self.vel
'''!@file           closedloopTP.py
    @brief          Implements a closed loop motor controller.
    @details        Allows the user to create a closed loop motor controller
                    object. This allows for the motor's duty cycle to be 
                    automatically changed until the platform has reached the
                    desired state. 
                    
                    For our closed loop control, we try to balance the platform
                    based on four things: angle, angular velocity, ball position,
                    and ball velocity. By taking all these factors into our 
                    calculation of motor duty cycle and using the proper gains,
                    we should be able to balance the platform using this controller.
                    
                    The closed loop class has two main methods, one for setting
                    the gains of the controller and one for calculating the
                    required duty cycle to level the platform and balance the ball.
                    
                    For a class diagram, reference the following:
                        
                    @image html TPCLCClass.JPG

    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''

class ClosedLoop:
    '''!@brief               Creates a motor controller object.
        @details             The closed loop class is used for closed loop motor
                             control. It contains two methods. The first is for 
                             running the controller to calculate the proper duty 
                             cycle to apply the necessary torque, and the second is for setting the
                             gains of the controller. There are four gains, Kpth,
                             Kdth, Kpx, and Kdx. Kpth and Kpx are proportional gains,
                             but Kpth is on the inner loop of control and is more
                             aggressive. Kdth and Kdx are derivative gains, but
                             again Kdth is on the inner loop and is higher.
        @author              Sean Wahl
        @author              Grant Gabrielson
        @author              Spencer Alonge
        @date                March 3, 2022
    '''
    
    def __init__ (self):
        '''!@brief          Initializes the motor controller.     
            @details        Nothing is needed to be inputted to create the motor
                            controller, however other methods need to be called
                            in order to actually use it.
        '''
        self.set_gain(0,0,0)
        pass
        
    def run (self, thMeas, wMeas, x, xdot):
        '''!@brief          Calculates the necessary duty cycle for the motor.    
            @details        Using the platform's angle and angular velocity along
                            with the ball's position and velocity, we are able
                            to calculate the required duty cycle for the motor.
                            The outer loop of control uses x and xdot to find a 
                            reference angle, and then feeds it to the inner loop
                            containing the angles in order to calculate the duty
                            cycle. With the right gains, this can level the platform.
            @param thMeas   The measured angle from the IMU.
            @param wMeas    The measured velocity from the IMU.
            @param x        The ball's position from the touchpad.  
            @param xdot     The ball's velocity from the touchpad.
            @return L       The required duty cycle (torque) for the motor.
        '''
        # Outer loop reference angle calculation
        thRef = -self.Kpx*x - self.Kdx*xdot
        
        # Outer loop angle saturation
        thRef = 15 if thRef > 15 else thRef
        thRef = -15 if thRef <-15 else thRef
        
        # Inner loop duty cycle calculation.
        self._temp = self.Kpth * (thRef - thMeas) - self.Kdth * wMeas

        # Duty cycle saturation limits.
        if self._temp > 50:
            ## @brief The duty cycle applied to the motor.
            #
            self.L = 50
        elif self._temp < -50:
            self.L = -50
        else:
            self.L = self._temp
            
        return self.L
    
    def set_gain (self, Kpx, Kdx, Kpth, Kdth):
        '''!@brief      Sets the gain for the controller.   
            @details    Sets the Kps (proportional) and Kds (derivative). The inner
                        loop gains should be greater than the outer loop gains
                        for an effective controller. There are two values for
                        each type of gain
            @param Kpx  Outer loop proportional gain on position.
            @param Kdx  Outer loop derivative gain on velocity.
            @param Kpth Inner loop proportional gain on angle.
            @param Kdth Inner loop derivative gain on angular velocity.
        '''
        
        # Set gains, set them to 0 if they're set below 0.
        ## @brief Outer loop proportional gain on position.
        #
        self.Kpx = 0 if Kpx < 0 else Kpx
        
        ## @brief Outer loop derivative gain on velocity.
        #
        self.Kdx = 0 if Kdx < 0 else Kdx
        
        ## @brief Inner loop proportional gain on angle.
        #
        self.Kpth = 0 if Kpth < 0 else Kpth
        
        ## @brief Inner loop derivative gain on angular velocity.
        #
        self.Kdth = 0 if Kdth < 0 else Kdth
        pass
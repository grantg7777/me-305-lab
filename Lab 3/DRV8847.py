'''!@file       DRV8847.py
    @brief      A general motor driver.
    @details    DRV8847 is a motor driver that is used to easily set up other
                motors with some advanced functionality and more ease of use
                than motor.py can provide. Mainly, the driver class is able
                to set up a timer, sleep pin, and fault pin that are usable for
                many motors. The timer and sleep pin are easier to use when
                defined here as it will be easier to call for them when creating
                a new motor object if they are already set up. The fault pin allows
                for advanced functionality in that the motors can now detect when
                a fault occurs (usually triggered by improper duty cycle changing)
                in either motor, and will stop the motors' operation until this
                fault is cleared. The sleep pin is used to enable and disable the
                motors.
                
                To see a class diagram listing all attributes and methods of
                DRV8847, reference the class diagram below:

                @image html Lab3DRVClass.JPG
    @author     Sean Wahl
    @author     Grant Gabrielson
    @date       February 15, 2022
'''

from pyb import Pin, ExtInt, Timer
from motor import Motor
from time import sleep_us, sleep

class DRV8847:
    '''!@brief               Creates a motor driver object.
        @details             A motor driver object is able to create motor
                             objects with a common timer, enable and disable
                             motors, and detect faults that cause the motor to
                             shut down. 
        @author              Sean Wahl
        @author              Grant Gabrielson
        @date                February 15, 2022
    '''
    
    def __init__ (self, timerObject, nSleepPin, nFaultPin):
        '''!@brief              Initializes the motor driver object.
            @details            By taking in a timer object and two pin objects,
                                a motor driver object can be created. The timer
                                object is used to create motors on this same
                                common timer. The pins are used to enable and
                                disable the driver as well as to detect faults
                                in the motors' operation.
            @param timerObject  A timer object used to create motors.
            @param nSleepPin    A pin object designating the sleep pin for motor
                                enabling and disabling.
            @param nFaultPin    A pin object designating the fault pin for motor
                                shut down on fault detection.
        '''
        ## The timer object designated by the user.
        #
        self.PWM_tim = timerObject
        ## @brief   Sleep pin object used for enabling/disabling motors.
        #  @details The sleep pin object linked to the sleep pin on the MCU 
        #           from the motor driver data sheet. Allows manipulation
        #           of when the motors can be used.
        #
        self.nSleep = nSleepPin
        
        ## @brief   Fault pin object used for fault detection on the motors.
        #  @details The fault pin object linked to the fault detection pin on 
        #           the MCU from the motor driver data sheet. Triggers when
        #           large duty cycle changes or other improper motor use occurs.
        #
        self.nFault = nFaultPin
        
        ## @brief       External interrupt object.
        #  @details     Sets up an external interrupt callback request upon fault
        #               detection. Triggered by the fault_cb method.
        #
        self.ButtonInt = ExtInt(nFaultPin, mode = ExtInt.IRQ_FALLING, pull = Pin.PULL_NONE, callback = self.fault_cb)
        
        ## Variable designating when an internal fault has occured.
        #
        self.faultState = False

    
    def enable(self):
        '''!@brief      Turns on the motors for use.
            @details    Manipulates the sleep pin in order to be able to use
                        the motors connected to this class. Disables interrupt
                        service request momentarily as faults can occur on
                        start up and should be ignored.
        '''
        self.ButtonInt.disable()
        self.nSleep.high()
        self.faultState = False
        sleep_us(50)
        self.ButtonInt.enable()
        pass
    
    def disable(self):
        '''!@brief      Turns off the motors.
            @details    Manipulates the sleep pin in order to shut off the motors,
                        preventing user control.
        '''
        self.nSleep.low()
        pass
    
    def fault_cb (self, IRQ_src):
        '''!@brief      Fault callback method for fault detection.
            @details    Upon an interrupt service request from the fault pin's
                        falling edge, this method will be called. It disables the
                        motors and sets an flag to true to signal a fault has
                        occured.
        '''        
        self.disable()
        self.faultState = True
        pass
    
    def motor(self, IN1_pin, IN2_pin, ch1, ch2):
        '''!@brief      Sets up a motor using the driver's timer and the motor class.
            @details    Takes in similar inputs needed to create a motor from the
                        motor class but without the timer object.
            @param IN1_pin  First pin object for motor control.
            @param IN2_pin  Second pin object for motor control.
            @param ch1      First timer channel number for motor use.
            @param ch2      Second timer channel number for motor use.
            @return         Motor object through calling the motor class.
        '''
        return Motor(self.PWM_tim, IN1_pin, IN2_pin, ch1, ch2)
    
    
if __name__ == '__main__':
    # testing interface to check functionality
    
    _tim = Timer(3, freq = 20000)
    _nSleep = Pin(Pin.cpu.A15, mode = Pin.OUT_PP)
    _nFault = Pin(Pin.cpu.B2, mode = Pin.IN)
    
    _motor_drv = DRV8847(_tim, _nSleep, _nFault)
    
    _motor_1 = _motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    _motor_2 = _motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    
    _motor_drv.enable()
    
    _motor_1.set_duty(50)
    _motor_2.set_duty(25)
    sleep(1)
    _motor_1.set_duty(0)
    _motor_2.set_duty(0)
    sleep(1)
    _motor_1.set_duty(100)
    _motor_2.set_duty(100)
    sleep(1)
    _motor_1.set_duty(-100)
    _motor_2.set_duty(-100)
    sleep(1)
    _motor_1.set_duty(0)
    _motor_2.set_duty(0)